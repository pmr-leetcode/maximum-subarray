package com.pajato.leetcode.maximumsubarray

fun maxSubArray(nums: IntArray): Int {
    var sum = 0
    nums.forEach { sum += it }
    return sum
}
