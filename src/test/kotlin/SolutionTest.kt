import com.pajato.leetcode.maximumsubarray.maxSubArray
import org.junit.Test
import kotlin.test.assertEquals

class SolutionTest {
    @Test fun `check out environment`() {
        assertEquals(4, 2 + 2)
    }

    @Test fun `check maximum sub-array function exists`() {
        val result = maxSubArray(intArrayOf())
    }

    @Test fun `when the sub array is 5 comma 7 test for output of 12`() {
        assertEquals(12, maxSubArray(intArrayOf(5, 7)))
    }

    @Test fun `when the sub array has 1 element return that element`() {
        assertEquals(1, maxSubArray(intArrayOf(1)))
    }

    @Test
    fun `when the sub array is -2 1 -3 4 -1 2 1 -5 4 the output should be 6`() {
        assertEquals( 6, maxSubArray(intArrayOf(-2,1,-3,4,-1,2,1,-5,4)))
    }
}